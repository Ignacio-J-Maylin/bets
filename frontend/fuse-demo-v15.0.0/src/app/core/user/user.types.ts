export interface User
{
    id: string;
    nombre: string;
    apellido: string;
    email: string;
    avatar?: string;
    status?: string;
}
