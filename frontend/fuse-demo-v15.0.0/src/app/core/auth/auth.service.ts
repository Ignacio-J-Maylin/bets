import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {of,switchMap, Observable } from 'rxjs';
import { environment } from 'environments/environment';
import { TokenStorageService } from './token-storage.service';

const AUTH_API =  environment.backendURL+'/api/auth/';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private _authenticated: boolean = false;
  constructor(private http: HttpClient, private tokenService: TokenStorageService,) { }

  signIn(credentials:{email: string, password: string}): Observable<any> {
    return this.http.post(AUTH_API + 'signin', credentials, httpOptions).pipe(switchMap((response: any) => {

      // Store the access token in the local storage
      this.tokenService.saveToken(response.accessToken);

      // Store the refresh token in the local storage
      this.tokenService.saveRefreshToken(response.refreshToken); 

        // Set the authenticated flag to true
        this._authenticated = true;

        // Store the user on the user service
        this.tokenService.saveUser(response.username);

        // Return a new observable with the response
        return of(response);
    })
);
}

  unlockSession(credentials: { email: string; password: string }): Observable<any>
     {
         return this.signIn(credentials);
       }

  signUp(user: {email: string, nombre: string, apellido: string, password: string,
    confirmPassword: string}): Observable<any> {
    return this.http.post(AUTH_API + 'signup',user, httpOptions);
  }
  registerAdmin(email: string, nombre: string, apellido: string, password: string,
    confirmPassword: string): Observable<any> {
    return this.http.post(AUTH_API + 'signup/admin', {
      email, nombre, apellido, password,confirmPassword
    }, httpOptions);
  }
  signOut(): Observable<any>
  {
      // Remove the access token from the local storage
      localStorage.removeItem('accessToken');

      // Set the authenticated flag to false
      this._authenticated = false;

      // Return the observable
      return of(true);
  }

  refreshToken(token: string) {
    return this.http.post(AUTH_API + 'refreshtoken', {
      refreshToken: token
    }, httpOptions);
  }

  
  resetPassword(password: string): Observable<any>
  {
      return this.http.post(AUTH_API + 'reset-password', password);
  }

  
  forgotPassword(email: string): Observable<any>
  {
      return this.http.post(AUTH_API + 'forgot-password', email);
  }
}
