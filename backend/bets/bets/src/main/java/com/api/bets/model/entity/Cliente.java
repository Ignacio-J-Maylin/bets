package com.api.bets.model.entity;

import com.api.bets.model.enums.Role;
import com.api.bets.model.request.NuevoClienteRequest;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "CLIENTE")
@PrimaryKeyJoinColumn(referencedColumnName = "EMAIL")
public class Cliente extends User {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4669620162348945576L;

	@Column(name = "DNI")
	private Integer dni;
	
	@Column(name = "TELEFONO")
	private Integer telefono;

	public Integer getDni() {
		return dni;
	}

	public void setDni(Integer dni) {
		this.dni = dni;
	}

	public Integer getTelefono() {
		return telefono;
	}

	public void setTelefono(Integer telefono) {
		this.telefono = telefono;
	}

	public Cliente() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	public Cliente(String email, String password, String nombre, String apellido, List<Roles> roles,
                   LocalDateTime fechaCreacion, LocalDateTime fechaModificacion, LocalDateTime fechaBaja) {
		super(email, password, nombre, apellido, roles, fechaCreacion, fechaModificacion, fechaBaja);
		// TODO Auto-generated constructor stub
	}



	public Cliente(String email, String password, String nombre, String apellido, List<Roles> roles,
                   LocalDateTime fechaCreacion, LocalDateTime fechaModificacion, LocalDateTime fechaBaja, Integer dni,
                   Integer telefono) {
		super(email, password, nombre, apellido, roles, fechaCreacion, fechaModificacion, fechaBaja);
		this.dni = dni;
		this.telefono = telefono;
	}

	public Cliente(NuevoClienteRequest nuevoCliente) {
		this.apellido = nuevoCliente.getApellido();
		this.nombre = nuevoCliente.getNombre();
		this.dni = 0;
		this.email = nuevoCliente.getEmail();
		this.telefono = 0;
		this.password = nuevoCliente.getPassword();
		this.fechaCreacion= LocalDateTime.now();
		List<Roles> roles = new ArrayList<Roles>();
		roles.add(new Roles(this, Role.ROLE_CLIENTE));
		this.setRoles(roles);
	}

}
