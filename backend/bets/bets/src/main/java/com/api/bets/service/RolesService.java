package com.api.bets.service;

import com.api.bets.model.entity.Admin;
import com.api.bets.model.entity.Cliente;
import com.api.bets.model.entity.Roles;
import com.api.bets.model.enums.Role;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface RolesService {

	@Transactional
	List<Roles> addNewRollClient(Cliente clientCreated);
	
	@Transactional
	void addNewRollAdmin(Admin AdminCreated);

	void inactivate(Roles oldRole);
	
	void activate(Roles oldRole);

	void addNewRoles(Admin Admin, Role newRole);

	void upgrade(Roles oldRoles, boolean contains);

	boolean existeUnAdmin();
	

}
