package com.api.bets.service.impl;

import com.api.bets.model.entity.Admin;
import com.api.bets.model.entity.Cliente;
import com.api.bets.model.entity.Roles;
import com.api.bets.model.enums.Role;
import com.api.bets.service.RolesService;
import com.api.bets.service.repository.RolesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class RolesServiceImpl implements RolesService {

	@Autowired
	private RolesRepository rolesRepo;

	@Override
	public List<Roles> addNewRollClient(Cliente clientCreated) {
		List<Roles> roles = new ArrayList<Roles>();
		roles.add(rolesRepo.save(new Roles(clientCreated, Role.ROLE_CLIENTE)));
		return roles;
	}

	@Override
	public void addNewRollAdmin(Admin adminCreated) {
		List<Roles> roles = new ArrayList<Roles>();
		roles.add(rolesRepo.save(new Roles(adminCreated, Role.ROLE_ADMIN)));
	}

	

	@Override
	public void addNewRoles(Admin admin, Role newRole) {
		List<Roles> roles = new ArrayList<Roles>();
		roles.add(rolesRepo.save(new Roles(admin, newRole)));
	}

	@Override
	public void upgrade(Roles oldRoles, boolean contains) {
		if (contains) {
			oldRoles.setAvailable(true);
			oldRoles.setModifiedDate(LocalDateTime.now());
		} else {
			oldRoles.setAvailable(false);
			oldRoles.setModifiedDate(LocalDateTime.now());

		}
		rolesRepo.save(oldRoles);
	}

	@Override
	public void activate(Roles oldRole) {
		oldRole.setAvailable(true);
		oldRole.setModifiedDate(LocalDateTime.now());
		rolesRepo.save(oldRole);
	}
	
	@Override
	public void inactivate(Roles oldRole) {
		oldRole.setAvailable(false);
		oldRole.setModifiedDate(LocalDateTime.now());
		rolesRepo.save(oldRole);
	}

	@Override
	public boolean existeUnAdmin() {
		List<Roles> adminRole = rolesRepo.findByRole(Role.ROLE_ADMIN);
		return !adminRole.isEmpty();
	}

}
