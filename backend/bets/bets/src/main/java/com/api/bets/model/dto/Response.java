package com.api.bets.model.dto;

public class Response {
	
	String mensaje;

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public Response() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Response(String mensaje) {
		super();
		this.mensaje = mensaje;
	}
	
}
