package com.api.bets.model.request;

import javax.validation.constraints.NotBlank;

public class NuevoClienteRequest {
	@NotBlank
	private String email;
	@NotBlank
	private String nombre;
	@NotBlank
	private String apellido;
	@NotBlank
	private String password;
	@NotBlank
	private String confirmPassword;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public NuevoClienteRequest(String email, String nombre, String apellido, String password, String confirmPassword) {
		super();
		this.email = email;
		this.nombre = nombre;
		this.apellido = apellido;
		this.password = password;
		this.confirmPassword = confirmPassword;
	}

	public NuevoClienteRequest() {
		super();
		// TODO Auto-generated constructor stub
	}

 

}
