package com.api.bets.model.request;

public class LogOutRequest {
  private String email;

  public String getEmail() {
    return this.email;
  }
}
