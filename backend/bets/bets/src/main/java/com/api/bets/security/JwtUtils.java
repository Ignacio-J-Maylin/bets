package com.api.bets.security;

import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class JwtUtils {
	private static final Logger logger = LoggerFactory.getLogger(JwtUtils.class);

	private  static final int jwtExpirationMs = 6000000;

	public String generateTokenFromUsername(UserDetails usuario) {
		List<String> roles = new ArrayList<String>();
		for (GrantedAuthority rol : usuario.getAuthorities()) {
			roles.add(rol.getAuthority());
		}
		List<GrantedAuthority> grantedAuthorities = AuthorityUtils
				.commaSeparatedStringToAuthorityList(getArrayToString(roles));
		String token = Jwts.builder().setId("softtekJWT").setSubject(usuario.getUsername())
				.claim("authorities",
						grantedAuthorities.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + jwtExpirationMs))
				.signWith(SignatureAlgorithm.HS512, AuthTokenFilter.SECRET.getBytes()).compact();
		return AuthTokenFilter.PREFIX + token;
	}

	public String getArrayToString(List<String> datos) {
		String texto = "";
		for (String dato : datos) {
			texto += dato + ",";
		}
		if (texto.length() > 1) {
			texto = texto.substring(0, texto.length() - 1);
		}
		return texto;
	}
//
//	public String generateTokenFromUsername(String username) {
//		return Jwts.builder().setSubject(username).setIssuedAt(new Date())
//				.setExpiration(new Date((new Date()).getTime() + jwtExpirationMs))
//				.signWith(SignatureAlgorithm.HS512, AuthTokenFilter.SECRET).compact();
//	}

	  public boolean validateJwtToken(String authToken) {
	    try {
	    	Jwts.parser().setSigningKey(AuthTokenFilter.SECRET.getBytes()).parseClaimsJws(authToken);
	      return true;
	    } catch (SignatureException e) {
	      logger.error("Invalid JWT signature: {}", e.getMessage());
	    } catch (MalformedJwtException e) {
	      logger.error("Invalid JWT token: {}", e.getMessage());
	    } catch (ExpiredJwtException e) {
	      logger.error("JWT token is expired: {}", e.getMessage());
	    } catch (UnsupportedJwtException e) {
	      logger.error("JWT token is unsupported: {}", e.getMessage());
	    } catch (IllegalArgumentException e) {
	      logger.error("JWT claims string is empty: {}", e.getMessage());
	    }

	    return false;
	  }

	public String generateTokenFromUsername(Authentication authentication) {
		List<String> roles = new ArrayList<String>();
		for (GrantedAuthority rol : authentication.getAuthorities()) {
			roles.add(rol.getAuthority());
		}
		List<GrantedAuthority> grantedAuthorities = AuthorityUtils
				.commaSeparatedStringToAuthorityList(getArrayToString(roles));
		String token = Jwts.builder().setId("softtekJWT").setSubject(authentication.getName())
				.claim("authorities",
						grantedAuthorities.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + jwtExpirationMs))
				.signWith(SignatureAlgorithm.HS512, AuthTokenFilter.SECRET.getBytes()).compact();
		return AuthTokenFilter.PREFIX + token;
	}
}
