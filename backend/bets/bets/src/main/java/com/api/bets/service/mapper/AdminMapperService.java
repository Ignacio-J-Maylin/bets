package com.api.bets.service.mapper;

import com.api.bets.model.entity.Admin;
import com.api.bets.model.entity.Roles;
import com.api.bets.model.enums.Role;
import com.api.bets.model.request.NuevoAdminRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class AdminMapperService {



    public Admin requestToEntity(NuevoAdminRequest nuevoAdmin) {
        Admin admin = new Admin();
        admin.setEmail(nuevoAdmin.getEmail());
        admin.setPassword(nuevoAdmin.getPassword());
        admin.setNombre(nuevoAdmin.getNombre());
        admin.setApellido(nuevoAdmin.getApellido());
        admin.setFechaCreacion(LocalDateTime.now());
        List<Roles> roles = new ArrayList<Roles>();
        roles.add(new Roles(admin, Role.ROLE_ADMIN));
        admin.setRoles(roles);
        return admin;
    }

}
