package com.api.bets.service.impl;
import com.api.bets.exception.LockedException;
import com.api.bets.exception.PreConditionFailedException;
import com.api.bets.model.entity.Admin;
import com.api.bets.model.entity.Cliente;
import com.api.bets.model.request.NuevoAdminRequest;
import com.api.bets.model.request.NuevoClienteRequest;
import com.api.bets.service.AdminService;
import com.api.bets.service.RolesService;
import com.api.bets.service.mapper.AdminMapperService;
import com.api.bets.service.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class AdminServiceImpl implements AdminService {

	@Autowired
	private UserRepository userRepository;
	@Autowired
	PasswordEncoder encoder;
	@Autowired
	private RolesService rolesService;
	@Autowired
	private AdminMapperService adminMapperService;


	public void crearCliente(NuevoClienteRequest nuevoCliente) throws PreConditionFailedException {
		existsByEmail(nuevoCliente.getEmail());
		checkPaswordEmail(nuevoCliente.getPassword(), nuevoCliente.getConfirmPassword());
		nuevoCliente.setPassword(encoder.encode(nuevoCliente.getPassword()));
		rolesService.addNewRollClient(userRepository.save(new Cliente(nuevoCliente)));
	}

	@Transactional
	@Override
	public void crearAdmin(NuevoAdminRequest nuevoAdmin) throws PreConditionFailedException {
		existsByEmail(nuevoAdmin.getEmail());
		checkPaswordEmail(nuevoAdmin.getPassword(), nuevoAdmin.getConfirmPassword());
		nuevoAdmin.setPassword(encoder.encode(nuevoAdmin.getPassword()));
		rolesService.addNewRollAdmin(userRepository.save(adminMapperService.requestToEntity(nuevoAdmin)));
	}

	private void checkPaswordEmail(String password, String confirmPassword) throws PreConditionFailedException {
		if (!password.equals(confirmPassword)) {
			throw new PreConditionFailedException(
					"Las contraseñas no coinciden. La contraseña y la confirmacion de esta " + "deben de ser iguales.");
		}

	}

	private void existsByEmail(String email) throws PreConditionFailedException {
		if (userRepository.existsByEmail(email)) {
			throw new PreConditionFailedException("Ese email está en uso");
		}
	}

	@Override
	public void comprobarQueNoExisteOtro() throws LockedException {
		if (rolesService.existeUnAdmin()) {
			throw new LockedException("Solo puede existir un admin y ya fue creado");
		}

	}

}
