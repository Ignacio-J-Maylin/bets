package com.api.bets.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.PRECONDITION_FAILED)
public class PreConditionFailedException extends Exception{

    private static final long serialVersionUID = 1L;

    public PreConditionFailedException(String message){
        super(message);
    }
}