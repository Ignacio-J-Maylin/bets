package com.api.bets.service.repository;


import com.api.bets.model.entity.Roles;
import com.api.bets.model.enums.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface RolesRepository extends JpaRepository<Roles, Long>{

	
	@Query("SELECT r from Roles r WHERE r.role= :role")
	List<Roles> findByRole(Role role);

}