package com.api.bets.controller;

import com.api.bets.model.request.*;
import com.api.bets.service.impl.RefreshTokenService;
import com.api.bets.service.repository.UserRepository;
import com.api.bets.exception.LockedException;
import com.api.bets.exception.PreConditionFailedException;
import com.api.bets.exception.ResourceNotFoundException;
import com.api.bets.exception.TokenRefreshException;
import com.api.bets.model.dto.JwtResponse;
import com.api.bets.model.dto.MessageResponse;
import com.api.bets.model.dto.Response;
import com.api.bets.model.dto.TokenRefreshResponse;
import com.api.bets.model.entity.RefreshToken;
import com.api.bets.model.entity.User;
import com.api.bets.service.repository.RolesRepository;
import com.api.bets.security.JwtUtils;
import com.api.bets.service.AdminService;
import com.api.bets.service.impl.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RolesRepository roleRepository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	UserDetailsServiceImpl userDetailsServiceImpl;

	@Autowired
	JwtUtils jwtUtils;

	@Autowired
	RefreshTokenService refreshTokenService;

	@Autowired
	private AdminService adminService;

	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest)
			throws LockedException, PreConditionFailedException {
		UserDetails userDetails = userDetailsServiceImpl.loadUserByUsername(loginRequest.getEmail());
		if (((User) userDetails).getFechaBaja() != null) {
			throw new LockedException("Ese cliente esta dado de baja.Si lo queres recuperar"
					+ " podes hablar con un administrador");
		}
		if (!encoder.matches(new StringBuffer(loginRequest.getPassword()), userDetails.getPassword())) {
			throw new PreConditionFailedException("La contraseña no es correcta. No recuerdas tu contraseña?");
		}

		String jwt = jwtUtils.generateTokenFromUsername(userDetails);

		List<String> roles = userDetails.getAuthorities().stream().map(item -> item.getAuthority())
				.collect(Collectors.toList());

		RefreshToken refreshToken = refreshTokenService.createRefreshToken(userDetails.getUsername());

		return ResponseEntity.ok(new JwtResponse(jwt, refreshToken.getToken(), userDetails.getUsername(), roles));
	}

	@PostMapping("/signup")
	public ResponseEntity<Response> crearCliente(@Valid @RequestBody NuevoClienteRequest nuevoCliente)
			throws ResourceNotFoundException, PreConditionFailedException, LockedException {
		adminService.crearCliente(nuevoCliente);
		Response msj= new Response("Cliente creado");
		return ResponseEntity.ok(msj);
	}
	@PostMapping("/signup/admin")
	public ResponseEntity<Response> crearAdmin(@RequestBody NuevoAdminRequest nuevoAdmin)
			throws ResourceNotFoundException, PreConditionFailedException, LockedException {
		
		adminService.comprobarQueNoExisteOtro();
		adminService.crearAdmin(nuevoAdmin);
		Response msj= new Response("Admin creado");
		return ResponseEntity.ok(msj);
	}

	@PostMapping("/refreshtoken")
	public ResponseEntity<?> refreshtoken(@Valid @RequestBody TokenRefreshRequest request) {
		String requestRefreshToken = request.getRefreshToken();

		return refreshTokenService.findByToken(requestRefreshToken)
				.map(refreshTokenService::verifyExpiration)
				.map(RefreshToken::getUser).map(user -> {
					String token = jwtUtils.generateTokenFromUsername(user);
					RefreshToken newRefreshToken = refreshTokenService.createRefreshToken(user.getEmail());
					return ResponseEntity.ok(new TokenRefreshResponse(token, newRefreshToken.getToken()));
				})
				.orElseThrow(() -> new TokenRefreshException(requestRefreshToken, "Refresh token es invalido!"));
	}

	@PostMapping("/logout")
	public ResponseEntity<?> logoutUser(@Valid @RequestBody LogOutRequest logOutRequest) {
		refreshTokenService.deleteByUserEmail(logOutRequest.getEmail());
		return ResponseEntity.ok(new MessageResponse("Deslogeado exitosamente!"));
	}

}
