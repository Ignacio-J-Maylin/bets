package com.api.bets.service;

import com.api.bets.exception.LockedException;
import com.api.bets.exception.PreConditionFailedException;
import com.api.bets.model.request.NuevoAdminRequest;
import com.api.bets.model.request.NuevoClienteRequest;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.List;


public interface AdminService {

	void crearCliente(NuevoClienteRequest nuevoCliente) throws PreConditionFailedException;

	void crearAdmin(NuevoAdminRequest nuevoAdmin) throws PreConditionFailedException;

	void comprobarQueNoExisteOtro() throws LockedException;


}
