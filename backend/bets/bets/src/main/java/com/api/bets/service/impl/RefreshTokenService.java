package com.api.bets.service.impl;

import com.api.bets.exception.TokenRefreshException;
import com.api.bets.model.entity.RefreshToken;
import com.api.bets.service.repository.RefreshTokenRepository;
import com.api.bets.service.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Optional;
import java.util.UUID;


@Service
public class RefreshTokenService {
	private  static final Long refreshTokenDurationMs = 1000000000L;

	@Autowired
	private RefreshTokenRepository refreshTokenRepository;

	@Autowired
	private UserRepository userRepository;

	public Optional<RefreshToken> findByToken(String token) {
		return refreshTokenRepository.findByToken(token);
	}

	public RefreshToken createRefreshToken(String email) {
		Optional<RefreshToken> opRefreshToken = refreshTokenRepository.findByUserEmail(email);
		
		if(opRefreshToken.isEmpty()) {

			RefreshToken refreshToken = new RefreshToken();

			refreshToken.setUser(userRepository.findOneByEmail(email).get());
			refreshToken.setExpiryDate(Instant.now().plusMillis(refreshTokenDurationMs));
			refreshToken.setToken(UUID.randomUUID().toString());

			refreshToken = refreshTokenRepository.save(refreshToken);
			return refreshToken;
		}
		else {
		RefreshToken refreshToken = opRefreshToken.get();

		refreshToken.setUser(userRepository.findOneByEmail(email).get());
		refreshToken.setExpiryDate(Instant.now().plusMillis(refreshTokenDurationMs));
		refreshToken.setToken(UUID.randomUUID().toString());

		refreshToken = refreshTokenRepository.save(refreshToken);
		return refreshToken;}
	}

	public RefreshToken verifyExpiration(RefreshToken token) {
		if (token.getExpiryDate().compareTo(Instant.now()) < 0) {
			refreshTokenRepository.delete(token);
			throw new TokenRefreshException(token.getToken(),
					"Refresh token was expired. Please make a new signin request");
		}

		return token;
	}

	@Transactional
	public int deleteByUserEmail(String email) {
		return refreshTokenRepository.deleteByUserEmail(userRepository.findOneByEmail(email).get());
	}

}
