package com.api.bets.model.request;

public class NuevoAdminRequest {
	private String email;

	private String nombre;
	
	private String apellido;

	private String password;
	
	private String confirmPassword;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public NuevoAdminRequest(String email, String nombre, String apellido, String password, String confirmPassword) {
		super();
		this.email = email;
		this.nombre = nombre;
		this.apellido = apellido;
		this.password = password;
		this.confirmPassword = confirmPassword;
	}

	public NuevoAdminRequest() {
		super();
		// TODO Auto-generated constructor stub
	}


}
