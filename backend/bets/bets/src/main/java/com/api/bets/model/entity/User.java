package com.api.bets.model.entity;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;


@Entity
@Table(name = "USER")
@Inheritance(strategy = InheritanceType.JOINED)
public class User implements UserDetails{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1132512238142594079L;

	@Id
	@Column(name = "EMAIL", nullable = false, unique = true)
	protected String email;
	
	@Column(name = "PASSWORD")
	protected String password;

	@Column(name = "NOMBRE")
	protected String nombre;

	@Column(name = "APELLIDO")
	protected String apellido;

	@OneToMany(mappedBy = "user")
	private List<Roles> roles = new ArrayList<Roles>();

	@Column(name = "FECHA_DE_CREACION")
	protected LocalDateTime fechaCreacion;

	@Column(name = "FECHA_DE_MODIFICACION")
	protected LocalDateTime fechaModificacion;

	@Column(name = "FECHA_DE_BAJA")
	protected LocalDateTime fechaBaja;



	public User(String email, String password, String nombre, String apellido, List<Roles> roles,
                LocalDateTime fechaCreacion, LocalDateTime fechaModificacion, LocalDateTime fechaBaja) {
		super();
		this.email = email;
		this.password = password;
		this.nombre = nombre;
		this.apellido = apellido;
		this.roles = roles;
		this.fechaCreacion = fechaCreacion;
		this.fechaModificacion = fechaModificacion;
		this.fechaBaja = fechaBaja;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public List<Roles> getRoles() {
		return roles;
	}

	public void setRoles(List<Roles> roles) {
		this.roles = roles;
	}

	public LocalDateTime getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(LocalDateTime fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public LocalDateTime getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(LocalDateTime fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public LocalDateTime getFechaBaja() {
		return fechaBaja;
	}

	public void setFechaBaja(LocalDateTime fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

	public User() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		
		return	this.roles.stream()
				.filter(role-> role.getAvailable())
				.collect(Collectors.toList());
			}

	@Override
	public String getUsername() {
		return this.email;
	}

	@Override
	public boolean isAccountNonExpired() {
		return this.fechaBaja.equals(null);
	}

	@Override
	public boolean isAccountNonLocked() {
		return this.fechaBaja.equals(null);
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return this.fechaBaja.equals(null);
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}




}
