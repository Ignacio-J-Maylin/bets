package com.api.bets.service.repository;


import com.api.bets.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface UserRepository extends JpaRepository<User, Long>{

	Optional<User> findOneByEmail(String email);

//	Optional<User> findOneByNickAndPassword(String nick, String password);

	boolean existsByEmail(String email);
	
//	@Query("SELECT u from User c where u.fechaBaja is null AND u.email = email")
//	User findUserByEmailAndFecheBajaIsNull(String email);

}