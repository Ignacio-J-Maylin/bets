package com.api.bets.service.repository;

import com.api.bets.model.entity.RefreshToken;
import com.api.bets.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface RefreshTokenRepository extends JpaRepository<RefreshToken, Long> {
	  Optional<RefreshToken> findByToken(String token);
	  
	  Optional<RefreshToken> findByUserEmail(String email);
  @Modifying
  int deleteByUserEmail(User user);
}
