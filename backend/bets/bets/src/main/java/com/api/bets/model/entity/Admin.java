package com.api.bets.model.entity;

import com.api.bets.model.enums.Role;
import com.api.bets.model.request.NuevoAdminRequest;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "ADMIN")
@PrimaryKeyJoinColumn(referencedColumnName = "EMAIL")
public class Admin extends User {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4091226458702445553L;

	@Column(name = "GANANCIA")
	private Double ganancia;

	public Double getGanancia() {
		return ganancia;
	}

	public void setGanancia(Double ganancia) {
		this.ganancia = ganancia;
	}



	public Admin() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Admin(String email, String password, String nombre, String apellido, List<Roles> roles,
                 LocalDateTime fechaCreacion, LocalDateTime fechaModificacion, LocalDateTime fechaBaja) {
		super(email, password, nombre, apellido, roles, fechaCreacion, fechaModificacion, fechaBaja);
		// TODO Auto-generated constructor stub
	}

	public Admin(String email, String password, String nombre, String apellido, List<Roles> roles,
                 LocalDateTime fechaCreacion, LocalDateTime fechaModificacion, LocalDateTime fechaBaja, Double ganancia) {
		super(email, password, nombre, apellido, roles, fechaCreacion, fechaModificacion, fechaBaja);
		this.ganancia = ganancia;
	}
	
	

}
